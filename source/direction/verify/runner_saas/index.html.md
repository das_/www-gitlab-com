---
layout: markdown_page
title: "Category Direction - Runner SaaS"
description: "This is the Product Direction Page for the Runner SaaS product category."
canonical_path: "/direction/verify/runner_saas/"
---

## Navigation & Settings

|                       |                               |
| -                     | -                             |
| Stage                 | [Verify](/direction/verify/)  |
| Maturity              | Viable |
| Content Last Reviewed | `2023-04-19`                  |

### Introduction and how you can help

Thanks for visiting this direction page on the Runner SaaS category at GitLab. This page belongs to the [Runner SaaS Group](https://about.gitlab.com/handbook/product/categories/#runner-saas-group) within the Verify Stage and is maintained by [Gabriel Engel](mailto:gengel@gitlab.com).

### Strategy and Themes

Runner SaaS is the product development effort for Runners on GitLab SaaS. Today those product offerings include:

- [GitLab SaaS Runners on Linux - GA](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html)
- [GitLab SaaS Runners on Windows - BETA](https://docs.gitlab.com/ee/ci/runners/saas/windows_saas_runner.html)
- [GitLab SaaS Runners on macOS M1 - BETA](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)

### 1 year plan

The primary FY2024 Runner themes focus on closing critical competitive gaps with other hosted solutions and also enable Ultimate adoption for GitLab SaaS, with the clear focus on achieving [best-in-class ci build speed](https://about.gitlab.com/company/team/structure/working-groups/ci-build-speed/).

- [GPU-enabled Runners](https://gitlab.com/groups/gitlab-org/-/epics/8648) for ModelOps workloads. (completed)
- [Add more powerful compute types for SaaS Runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8714).
- [macOS](https://gitlab.com/groups/gitlab-org/-/epics/8267) and [Windows](https://gitlab.com/gitlab-org/gitlab/-/issues/300476) runners transition to General Availability.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next three months we plan to complete the following projects:

- Release more [powerful compute types for SaaS Runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8714).
- Transition our [macOS Runners to General Availability](https://gitlab.com/groups/gitlab-org/-/epics/8267).

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

- We are working to further expand our Linux runner portfolio by [offering more powerful compute](https://gitlab.com/groups/gitlab-org/-/epics/8714).
- Also we are focused to bring our newly release [Apple silicon (M1) runners on macOS to General Availability](https://gitlab.com/groups/gitlab-org/-/epics/8267).


#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- We have doubled our existing [SaaS runners on Linux](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#upsizing-gitlab-saas-runners-on-linux) in vCPU and RAM with no increase in costfactor.
- We have released [GPU-enabled runners on Linux](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#gpu-enabled-saas-runners-on-linux) for ModelOps and HPC workloads.
- We have transitioned to [Apple silicon (M1) runners on macOS](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#apple-silicon-m1-gitlab-saas-runners-on-macos-beta) with up to three times the performance of hosted x86-64 macOS runners.

#### What is Not Planned Right Now

We are not actively working on the following:

- [GitLab SaaS Runners on Windows - GA](https://gitlab.com/gitlab-org/gitlab/-/issues/300476)
- [GitLab Arm-based SaaS Runners on Linux](https://gitlab.com/gitlab-org/gitlab/-/issues/388320)
- [SaaS Runners for GitLab Self-Managed & Dedicated](https://gitlab.com/gitlab-org/gitlab/-/issues/385199)

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities

Organizations that use Cloud-native CI/CD solutions, such as GitLab.com, Harness.io, CircleCI, and GitHub, can run their CI/CD pipelines and get to a first green build without setting up build servers, installing and configuring build agents, or runners.

In addition to eliminating CI build server maintenance costs, there are other critical considerations for organizations that can migrate 100% of their CI/CD processes to a cloud-native solution. These include security, reliability, performance, multiple build server and configuration options, and on-demand scale. A critical strategic goal is to deliver solutions on GitLab SaaS that meet the most stringent compliance requirements and provides methods for securely connecting the GitLab SaaS Runner platform with a customer infrastructure. This, we believe, is the approach that will unlock the most value long-term for GitLab customers.

CI build speed or time-to-result, and the related CI build infrastructure cost efficiency are now critical competitive vectors. [CircleCI](https://circleci.com/circleci-versus-github-actions/) and [Harness.io](https://www.harness.io/blog/announcing-speed-enhancements-and-hosted-builds-for-harness-ci) are promoting CI-build performance in their go-to-market strategy.

Other industry participants, such as GitHub, have also invested in providing more computing choices for their hosted offerings. The recent [announcement](https://github.blog/2022-09-01-github-actions-introducing-the-new-larger-github-hosted-runners-beta/) by GitHub that new larger GitHub-hosted runners are now in beta, signals that the market is adopting CircleCI's position of providing more computing choices for customers that use SaaS-delivered CI/CD solutions. Finally, in recent blog posts, both [GitHub](https://github.blog/2022-12-08-experiment-the-hidden-costs-of-waiting-on-slow-build-times/) and [Harness.io](https://www.harness.io/blog/fastest-ci-tool) explored the cost impact of CI build performance measured by build times on hosted solutions.

#### Top [1/2/3] Competitive Solutions

**Hosted Build Environment Resource Classes - Linux non GPU-enabled**

|Machine Class| Machine Type/Class | GitLab | GitHub | CircleCI |
| ------ | :-----: |  :-----: |  :-----:|:-----:|
|S| 2 vCPUs, 8GB RAM | Available | Available | Available |
|M| 4 vCPUs, 16GB RAM  | Available | Beta  | Available |
|L| 8 vCPUs, 32GB RAM  | Available | Beta| Available |
|XL|16 vCPUs 64GB RAM| Not available | Beta| Available |
|2XL|32 vCPUs 128GB RAM| Not available | Beta | Not available |
|3XL|48 vCPUs 192GB RAM| Not available | Beta | Not available |
|4XL|64 vCPUs 256GB RAM| Not available | Beta | Not available |

**macOS - Offer Positioning and Hosted Build Machines**

|....||GitHub|Apple - Xcode Cloud GA |CircleCI|Bitrise.io|
|----------|----------------|----------------|----------------|----------------|----------------|
|Positioning Statement ||A GitHub-hosted runner is  VM hosted by GitHub with the GitHub actions runner application installed.|A CI/CD service built into Xcode, designed expressly for Apple developers.|Industry-leading speed. No other CI/CD platform takes performance as seriously as we do.|Build better mobile applications, faster|
|Value Proposition||When you use a GitHub-hosted runner, machine maintenance and upgrades are taken care of.|Build your apps in the cloud and eliminate dedicated build infrastructure.| The macOS execution environment allows you to test, build, and deploy macOS and iOS apps on CircleCI.|CI for mobile - save time spent on testing, onboarding, and maintenance with automated workflows and triggers|
|macOS Virtual Machine Specs||3-core CPU, 14 GB RAM |--|Medium: 4 vCPU, 8 GB RAM; Large: 8 vCPU, 16 GB RAM;  Metal 12 vCPU 32 GB RAM|Standard: 4 vCPU, 19 GB RAM; Elite 8 vCPU 35 GB ram; Elite XL 12 vCPU 54 GB RAM|

### Maturity Plan

- Runner SaaS is at the "Viable" maturity level - see our [definitions of maturity levels](/direction/maturity/).
